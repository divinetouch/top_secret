require 'rails_helper'

RSpec.describe DocumentsController, type: :controller do

  before(:each) do
    @user1 = create(:user)
    @document1 = create(:document)
  end
  after(:each) do
    @document1.delete
    @user1.delete
  end

  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #show" do
    it "returns http success" do
      sign_in @user1
      get :show, params: {id: @document1.id}
      expect(response).to have_http_status(:success)
    end
  end

end

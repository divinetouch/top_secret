require 'rails_helper'

RSpec.describe User, type: :model do
  before(:each) do
    @user1 = create(:user)
  end
  after(:each) do
    @user1.delete
  end
  it "is valid with valid attributes" do
    expect(@user1).to be_valid
  end
  it 'should extract out domain from email' do
    expect(@user1.domain).to eq(@user1.email.split('@').last)
  end
end

require 'rails_helper'

RSpec.describe Company, type: :model do
  before(:each) do
    @company1 = create(:company)
  end
  after(:each) do
    @company1.delete
  end

  it "is valid with valid attributes" do
    expect(@company1).to be_valid
  end
end

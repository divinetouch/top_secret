require 'rails_helper'

RSpec.describe Document, type: :model do
  before(:each) do
    @document1 = create(:document)
  end
  after(:each) do
    @document1.delete
  end

  it "is valid with valid attributes" do
    expect(@document1).to be_valid
  end
  it "is valid with valid company attributes" do
    expect(@document1.company).to be_valid
  end
end

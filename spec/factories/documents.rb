FactoryBot.define do
  factory :document do
    name 'Spacex document'
    content {FFaker::Lorem.paragraph}
    association :company
  end
end

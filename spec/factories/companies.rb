FactoryBot.define do
  factory :company do
    name 'SpaceX'
    domain 'spacex.com'
  end
end

FactoryBot.define do
  factory :user do
    email {FFaker::Name.first_name+'@spacex.com'}
    password {Devise.friendly_token.first(8)}
  end
end

Rails.application.routes.draw do
  devise_for :users
  # get 'documents/index'
  # get 'documents/show'
  resources :documents, only: [:show, :edit, :update]
  root 'documents#index'
  get '/documents' => redirect('/')
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
